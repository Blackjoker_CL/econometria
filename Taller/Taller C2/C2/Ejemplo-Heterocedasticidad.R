
rm(list=ls())

work.dir<-"F://Econometrics//Taller//Taller C2//C2"
setwd(work.dir)
data<-read.table("Data2.txt",header=TRUE)

model.1<-lm(Y~X1+X2+X3+X4+X5+TIME,data=data)
print(summary(model.1),digits=4)

PIBR = data$X2/data$X1

model.2<-lm(Y~PIBR+X4+X5,data=data)
summary(model.2)

model.3<-lm(Y~PIBR+X3+X4+X5+TIME,data=data)
summary(model.3)

model.4<-lm(Y~PIBR+X3+X4+X5,data=data)
summary(model.4)


data<-read.table("Data3.txt",header=TRUE)

model.1<-lm(IyD~Ventas,data=data)
summary(model.1)

model.2<-lm(IyD~Ventas+Utilidades,data=data)
summary(model.2)

ehat<-model.2$residuals
ehat2<-ehat**2
ehata<-abs(ehat)

model.3<-lm(ehat2~Ventas+Utilidades,data=data)
summary(model.3)

model.4<-lm(ehata~Ventas+Utilidades,data=data)
summary(model.4)

Ventas2 <- data$Ventas**2
model.5<-lm(ehat2~Ventas+Ventas2,data=data)
summary(model.5)

model.6<-lm(ehat2~Ventas,data=data)
summary(model.6)

data<-read.table("Data4.txt",header=TRUE,dec=",")
names(data)

lnCt<-log(data$C)
lnYd<-log(data$Yd)
lnW<-log(data$W)

model.7<-lm(lnCt~lnYd+lnW+I,data=data)
summary(model.7)
ehat<-model.7$residuals
par(mfrow=c(2,1))
acf(model.7$residuals)
pacf(model.7$residuals)

arima(ehat,c(1,0,0))
arima(ehat,c(0,0,1))
arima(ehat,c(1,0,1))





names(data)[1]<-"Airline"
names(data)[2]<-"Year"
names(data)[3]<-"Output"
names(data)[4]<-"TotalCost"
names(data)[5]<-"FuelPrice"
names(data)[6]<-"LoadFactor"

plot.ts(data[,3:6])
plot(data$TotalCost,data$Output)
plot(data$FuelPrice,data$Output)
plot(data$LoadFactor,data$Output)
plot(data$Airline,data$Output)

logcost    = log(data[,4])
logoutput  = log(data[,3])
logfuel    = log(data[,5])
loadfactor = data[,6]

model.1<-lm(logcost~logoutput+logfuel+loadfactor)
summary(model.1)

########################
# Ordinary least squares
########################
n     = nrow(data)
X     = cbind(1,logoutput,logfuel,loadfactor)
k     = ncol(X)
y     = logcost
iXtX  = solve(t(X)%*%X)
bhat  = iXtX%*%t(X)%*%y
yhat  = X%*%bhat
ehat  = y-yhat
s2hat = sum(ehat^2)/(n-k)
se    = sqrt(diag(s2hat*iXtX))

round(cbind(bhat,se,bhat/se,2*(1-pt(abs(bhat/se),n-k))),5)


# Test de Spearman
cor.test(logoutput, logcost, method = "spearm")
cor.test(logfuel, logcost, method = "spearm")
cor.test(loadfactor, logcost, method = "spearm")


########################
# Teste de Breusch-Pagan
########################
ehat2 = ehat^2
summary(lm(ehat2~X-1))

# F-statistic: 12.44 on 4 and 86 DF,  p-value: 4.981e-08


########################
# Teste de White
########################
x1 = logoutput
x2 = logfuel
x3 = loadfactor
x1x2 = x1*x2
x1x3 = x1*x3
x2x3 = x2*x3
x12  = x1^2
x22  = x2^2
x32  = x3^2
summary(lm(ehat2~x1+x2+x3+x12+x22+x32+x1x2+x1x3+x1x3))

#F-statistic: 6.948 on 8 and 81 DF,  p-value: 6.189e-07


####################################
# Teste de White: regressao auxiliar
####################################
yhat2 = yhat^2
summary(lm(ehat2~yhat+yhat2))

#F-statistic: 17.91 on 2 and 87 DF,  p-value: 3.054e-07


###########################################
# White standard error (robust estimator)
###########################################
Omega = diag(ehat2[,1])
se.white = sqrt(diag(iXtX%*%t(X)%*%Omega%*%X%*%iXtX))

cbind(bhat,se,se.white)


###########################################
# Generalized least square (GLS) estimator
###########################################
Psi    = diag(sqrt(1/ehat2[,1]))
y1     = Psi%*%y
X1     = Psi%*%X
iXtX1  = solve(t(X1)%*%X1)
bhat1  = iXtX1%*%t(X1)%*%y1
se.gls = sqrt(diag(iXtX1)/s2hat)

cbind(bhat,se,se.white,bhat1,se.gls)

summary(lm(y1~X1))


###########################################
# Heterocedasticity of known form
###########################################
par(mfrow=c(1,1))
plot(loadfactor,ehat,xlim=c(0.4,0.7),ylim=c(-0.4,0.4),
     xlab="Load factor, the average capacity utilization of the fleet",
     ylab="Residuals")
abline(h=0,lty=2)
segments(0.4,0,0.6,0.4,lty=2,col=2)
segments(0.4,0,0.6,-0.4,lty=2,col=2)

plot(loadfactor,log(ehat^2),
     xlab="Load factor, the average capacity utilization of the fleet",
     ylab="Log Squared Residuals")
abline(lm(log(ehat^2)~loadfactor),col=2)

summary(lm(log(ehat^2)~loadfactor))







