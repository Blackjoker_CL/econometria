
rm(list=ls())

work.dir<-"F://Econometrics//Taller//Taller C2//ExC11"
setwd(work.dir)
data<-read.table("CAFE.txt",header=TRUE)


model.1<-lm(q~Y+pc,data=data)
summary(model.1)


model.2<-lm(q~Y+pc+pl,data=data)
summary(model.2)

cor(data$pc,data$pl)

out.pc<-princomp(data[3:5],cor=TRUE)
summary(out.pc)
loadings(out.pc)
C1<- as.matrix(data[3:5]) %*% as.matrix(out.pc$loadings[,1])
C2<- as.matrix(data[3:5]) %*% as.matrix(out.pc$loadings[,2])
C3<- as.matrix(data[3:5]) %*% as.matrix(out.pc$loadings[,3])

cor(C1,C2)
cor(C1,C3)
cor(C2,C3)

Indice<- 0.5*C1 + 0.5*C2

model.3<-lm(q~Y+Indice,data=data)
summary(model.3)

library(leaps)
outs <- leaps(X, y, int = FALSE, strictly.compatible = FALSE)
par(mfrow=c(1,1))
plot(outs$size, outs$Cp, log = "y", xlab = "p", ylab = expression(C[p]))
lines(outs$size, outs$size)



########################
# Ordinary least squares
########################
n     = nrow(data)
X     = cbind(1,data$Y,data$pc)
k     = ncol(X)
y     = data$q
iXtX  = solve(t(X)%*%X)
bhat  = iXtX%*%t(X)%*%y
yhat  = X%*%bhat
ehat  = y-yhat
s2hat = sum(ehat^2)/(n-k)
se    = sqrt(diag(s2hat*iXtX))

round(cbind(bhat,se,bhat/se,2*(1-pt(abs(bhat/se),n-k))),5)
ehat2 = ehat^2



########################
# Test de Spearman
########################

cor.test(data$Y, ehat2, method = "spearm")
cor.test(data$pc, ehat2, method = "spearm")

par(mfrow=c(2,1))
plot(data$Y, ehat2)
lines(lowess(ehat2~data$Y),col="red")
plot(data$pc, ehat2)
lines(lowess(ehat2~data$pc),col="red")

########################
# Teste de Breusch-Pagan
########################

summary(lm(ehat2~X-1))

# F-statistic: 1.272 on 3 and 17 DF,  p-value: 0.3157


########################
# Teste de White
########################
x1 = data$Y
x2 = data$pc
x1x2 = x1*x2
x12  = x1^2
x22  = x2^2
summary(lm(ehat2~x1+x2+x12+x22+x1x2))

#F-statistic: 6.948 on 8 and 81 DF,  p-value: 6.189e-07


####################################
# Teste de White: regressao auxiliar
####################################
yhat2 = yhat^2
summary(lm(ehat2~yhat+yhat2))

#F-statistic: 17.91 on 2 and 87 DF,  p-value: 3.054e-07


###########################################
# White standard error (robust estimator)
###########################################
Omega = diag(ehat2[,1])
se.white = sqrt(diag(iXtX%*%t(X)%*%Omega%*%X%*%iXtX))

cbind(bhat,se,se.white)


###########################################
# Generalized least square (GLS) estimator
###########################################
Psi    = diag(sqrt(1/ehat2[,1]))
y1     = Psi%*%y
X1     = Psi%*%X
iXtX1  = solve(t(X1)%*%X1)
bhat1  = iXtX1%*%t(X1)%*%y1
se.gls = sqrt(diag(iXtX1)/s2hat)

cbind(bhat,se,se.white,bhat1,se.gls)

summary(lm(y1~X1))


###########################################
# Heterocedasticity of known form
###########################################
par(mfrow=c(1,1))
plot(loadfactor,ehat,xlim=c(0.4,0.7),ylim=c(-0.4,0.4),
     xlab="Load factor, the average capacity utilization of the fleet",
     ylab="Residuals")
abline(h=0,lty=2)
segments(0.4,0,0.6,0.4,lty=2,col=2)
segments(0.4,0,0.6,-0.4,lty=2,col=2)

plot(loadfactor,log(ehat^2),
     xlab="Load factor, the average capacity utilization of the fleet",
     ylab="Log Squared Residuals")
abline(lm(log(ehat^2)~loadfactor),col=2)

summary(lm(log(ehat^2)~loadfactor))

ts.plot(ehat)
par(mfrow=c(2,1))
acf(ehat)
pacf(ehat)




